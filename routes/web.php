<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// ini template master
// Route::get('/master', function () {
//     return view('layouts/master');
// });


// route home
Route::any('/', 'WebController@home');

Route::any('/data-tables', 'WebController@dataItems');



